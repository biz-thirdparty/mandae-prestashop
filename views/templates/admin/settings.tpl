{*
* 2007-2011 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2014 PrestaShop SA
*  @version  Release: $Revision: 6594 $
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script xmlns="http://www.w3.org/1999/html">
    if (!window.jQuery) {
        var script = document.createElement('script');
        script.type = "text/javascript";
        script.src = "{$module_dir|escape:'none'}assets/js/jquery.min.js";
        document.getElementsByTagName('head')[0].appendChild(script);
    }
</script>

<div class="panel-body">
    <div class="row admin-title-row">
        <div class="pull-left">
            <a target="_BLANK" href="http://www.mandae.com.br/">
                <img src="{$module_dir|escape:'none'}assets/images/photo.jpg" alt="mandaê" title="mandaê"/>
            </a>
        </div>
    </div>

    {if $action == 'update'}
        <div class="bootstrap">
            <div class="module_confirmation conf confirm alert alert-success">
                <button type="button" class="close" data-dismiss="alert">×</button>
                {l s='Pedidos Atualizados com Sucesso' mod='mandae'}
            </div>
        </div>
    {/if}

    <br>
    <br>

    <form action="{$action_post|escape:'none'}" method="POST" enctype="multipart/form-data" id="form-std-uk"
          class="form-horizontal">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab-general" data-toggle="tab">{l s='Configurações Gerais' mod='mandae'}</a>
            </li>
            <li>
                <a href="#tab-un" data-toggle="tab">{l s='Unidades de Medida' mod='mandae'}</a>
            </li>
            <li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="tab-general">
                <div class="form-group">
                    <label class="col-sm-3 control-label">
                        {l s='Ambiente' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <div class="pull-left admin-item-form">
                            <label>
                                <input type="radio" name="MANDAE_SANDBOX" value="1" {if $MANDAE_SANDBOX} checked {/if}>
                                {l s='Testes' mod='mandae'}
                            </label>
                        </div>

                        <div class="pull-left admin-item-form">
                            <label>
                                <input type="radio" name="MANDAE_SANDBOX" value="0" {if !$MANDAE_SANDBOX} checked {/if}>
                                {l s='Produção' mod='mandae'}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="divisor"></div>

                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_STATUS">
                        {l s='Status' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <select type="text" name="MANDAE_STATUS" id="MANDAE_STATUS"
                                class="form-control">
                            <option value="enabled" {if $MANDAE_STATUS eq 'enabled'} selected {/if}>{l s='Habilitado' mod='mandae'}</option>
                            <option value="disabled" {if $MANDAE_STATUS eq 'disabled'} selected {/if}>{l s='Desabilitado' mod='mandae'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_TOKEN">
                        {l s='Token' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="MANDAE_TOKEN"
                               value="{$MANDAE_TOKEN}" id="MANDAE_TOKEN"
                               class="form-control"/>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_METHOD_TITLE">
                        {l s='Título do Método de Envio' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="MANDAE_METHOD_TITLE"
                               value="{$MANDAE_METHOD_TITLE}" id="MANDAE_METHOD_TITLE"
                               class="form-control"/>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_DECLARED_VALUE">
                        {l s='Usar Valor Declarado' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <select type="text" name="MANDAE_DECLARED_VALUE" id="MANDAE_DECLARED_VALUE"
                                class="form-control">
                            <option value="yes" {if $MANDAE_DECLARED_VALUE eq 'yes'} selected {/if}>{l s='Sim' mod='mandae'}</option>
                            <option value="no" {if $MANDAE_DECLARED_VALUE eq 'no'} selected {/if}>{l s='Não' mod='mandae'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_ADDITIONAL_DEADLINE">
                        {l s='Prazo Adicional' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="MANDAE_ADDITIONAL_DEADLINE"
                               value="{$MANDAE_ADDITIONAL_DEADLINE}" id="MANDAE_ADDITIONAL_DEADLINE"
                               class="form-control"/>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_HANDLING_TYPE">
                        {l s='Tipo de Taxa de Manuseio' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <select type="text" name="MANDAE_HANDLING_TYPE" id="MANDAE_HANDLING_TYPE"
                                class="form-control">
                            <option value="none" {if $MANDAE_HANDLING_TYPE eq 'none '} selected {/if}>{l s='Nenhuma' mod='mandae'}</option>
                            <option value="percentage" {if $MANDAE_HANDLING_TYPE eq 'percentage'} selected {/if}>{l s='Percentual' mod='mandae'}</option>
                            <option value="fixed" {if $MANDAE_HANDLING_TYPE eq 'fixed'} selected {/if}>{l s='Fixa' mod='mandae'}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_HANDLING_FEE">
                        {l s='Taxa de Manuseio' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <input type="text" name="MANDAE_HANDLING_FEE"
                               value="{$MANDAE_HANDLING_FEE}" id="MANDAE_HANDLING_FEE"
                               class="form-control"/>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="col-sm-3 control-label" for="MANDAE_DEBUG">
                        {l s='Habilitar Log' mod='mandae'}
                    </label>
                    <div class="col-sm-9">
                        <select type="text" name="MANDAE_DEBUG" id="MANDAE_DEBUG"
                                class="form-control">
                            <option value="enabled" {if $MANDAE_ENABLE_LOG eq 'enabled'} selected {/if}>{l s='Habilitado' mod='mandae'}</option>
                            <option value="disabled" {if $MANDAE_ENABLE_LOG eq 'disabled'} selected {/if}>{l s='Desabilitado' mod='mandae'}</option>
                        </select>
                    </div>
                </div>

                <div class="divisor"></div>

        </div>

                <div class="tab-pane" id="tab-un">
                    <div class="form-group required">

                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_VALUES">
                            {l s='Usar Valores Padrão para Dimensões' mod='mandae'}
                        </label>

                        <div class="col-sm-9">
                            <select type="text" name="MANDAE_DEFAULT_VALUES" id="MANDAE_DEFAULT_VALUES"
                                    class="form-control">
                                <option value="yes" {if $MANDAE_DEFAULT_VALUES eq 'yes'} selected {/if}>{l s='Sim' mod='mandae'}</option>
                                <option value="no" {if $MANDAE_DEFAULT_VALUES eq 'no'} selected {/if}>{l s='Não' mod='mandae'}</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_WIDTH">
                            {l s='Valor Padrão para Largura' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="MANDAE_DEFAULT_WIDTH"
                                   value="{$MANDAE_DEFAULT_WIDTH}" id="MANDAE_DEFAULT_WIDTH"
                                   class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_HEIGHT">
                            {l s='Valor Padrão para Altura' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="MANDAE_DEFAULT_HEIGHT"
                                   value="{$MANDAE_DEFAULT_HEIGHT}" id="MANDAE_DEFAULT_HEIGHT"
                                   class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_DEPTH">
                            {l s='Valor Padrão para Profundidade' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="MANDAE_DEFAULT_DEPTH"
                                   value="{$MANDAE_DEFAULT_DEPTH}" id="MANDAE_DEFAULT_DEPTH"
                                   class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_WEIGHT">
                            {l s='Valor Padrão para Peso' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <input type="text" name="MANDAE_DEFAULT_WEIGHT"
                                   value="{$MANDAE_DEFAULT_WEIGHT}" id="MANDAE_DEFAULT_WEIGHT"
                                   class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_UNITY">
                            {l s='Unidade Padrão para Comprimento' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <select type="text" name="MANDAE_DEFAULT_UNITY" id="MANDAE_DEFAULT_UNITY"
                                    class="form-control">
                                <option value="cm" {if $MANDAE_DEFAULT_UNITY eq 'cm'} selected {/if}>{l s='Centímetros' mod='mandae'}</option>
                                <option value="m" {if $MANDAE_DEFAULT_UNITY eq 'm'} selected {/if}>{l s='Metros' mod='mandae'}</option>
                                <option value="in" {if $MANDAE_DEFAULT_UNITY eq 'pol'} selected {/if}>{l s='Polegadas' mod='mandae'}</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group required">
                        <label class="col-sm-3 control-label" for="MANDAE_DEFAULT_WEIGHT_UNITY">
                            {l s='Unidade Padrão para Peso' mod='mandae'}
                        </label>
                        <div class="col-sm-9">
                            <select type="text" name="MANDAE_DEFAULT_WEIGHT_UNITY" id="MANDAE_DEFAULT_WEIGHT_UNITY"
                                    class="form-control">
                                <option value="kg" {if $MANDAE_DEFAULT_WEIGHT_UNITY eq 'kg'} selected {/if}>{l s='Quilo' mod='mandae'}</option>
                                <option value="g" {if $MANDAE_DEFAULT_WEIGHT_UNITY eq 'g'} selected {/if}>{l s='Grama' mod='mandae'}</option>
                                <option value="lb" {if $MANDAE_DEFAULT_WEIGHT_UNITY eq 'lb'} selected {/if}>{l s='Libra' mod='mandae'}</option>
                            </select>
                        </div>
                    </div>
                </div>

            <div id="divSalvar">
            <input type="submit" class="btn btn-success admin-btn" name='btnSubmit' value="{l s='Salvar Alterações' mod='mandae'}"/>
        </div>

    </form>
</div>
