{*
* 2007-2018 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author maxiPago!
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script>
document.addEventListener('DOMContentLoaded', function(event) {
    var deliveryDays = {$delivery_days nofilter};
    var addDeadline = parseInt('{$add_deadline}');

    jQuery('.delivery_options .delivery_option strong').each(function(i, el){
        var methodName = jQuery(el).html();
        if (methodName.indexOf('Mandaê') > -1) {

            jQuery(deliveryDays).each(function(index, method){
                if (method.name == methodName.replace('Mandaê - ', '')) {
                    var newDeliveryTime = jQuery(el).parent().html().replace('%d', addDeadline + method.days);
                    jQuery(el).parent().html(newDeliveryTime);
                }
            });

        }

    });

    jQuery('.delivery-options .delivery-option').each(function(i, el) {
        var methodName = jQuery(el).find('.carrier-name').html();
        if (methodName.indexOf('Mandaê') > -1) {
            jQuery(deliveryDays).each(function (index, method) {
                if (method.name == methodName.replace('Mandaê - ', '')) {
                    var newDeliveryTime = jQuery(el).find('.carrier-delay').html().replace('%d', addDeadline + method.days);
                    jQuery(el).find('.carrier-delay').html('Prazo de Entrega: ' +   newDeliveryTime);
                }
            });

        }
    });
});
</script>
