<div class="mandae-tracking-info tracking-info">
    <p>
        <img src="{$logo}"/>
    </p>
    <label class="tracking-progress-label">
        {l s='Progresso da Encomenda #'}
    </label>
    <label class="code-label">{$trackingCode}</label>
    <p>
    {if $events}
    <table>
        <thead>
        <tr class="sucess-event">
            <th>{l s='Data'}</th>
            <th>{l s='Nome'}</th>
            <th>{l s='Descrição'}</th>
        </tr>
        </thead>
        {foreach $events as $event}
            <tbody>
            <tr class="sucess-event">
                <td>{$event->date}</td>
                <td>{$event->name}</td>
                <td>{$event->description}</td>
            </tr>
            </tbody>
        {/foreach}
    </table>
    {else}
        {if $error}
            <p class="error-message">{$error}</p>
        {/if}
    {/if}
</div>