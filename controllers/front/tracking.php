<?php
/**
 * Created by PhpStorm.
 * User: monicacolomera
 * Date: 12/01/18
 * Time: 13:57
 */

require_once(dirname(__FILE__).'/../../classes/MandaeService.php');

class MandaeTrackingModuleFrontController extends ModuleFrontController
{
    protected $name = 'mandae';

    const URL_API = 'https://api.mandae.com.br/v2/';
    const SANDBOX_URL_API = 'https://sandbox.api.mandae.com.br/v2/';

    public function init()
    {
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->displayFooter(true);
            $this->displayHeader(true);
        } else {
            $this->display_header = true;
            $this->display_footer = true;

            $this->display_column_left = false;
            $this->display_column_right = false;
        }

        parent::init();
    }

    public function initContent()
    {
        parent::initContent();

        $trackingCode = Tools::getValue('object');
        $url = MandaeService::getUrl() . 'trackings/' . $trackingCode;

        $response = json_decode(MandaeService::request($url));

        $errorMessage = null;
        $events = array();

        if (property_exists($response, 'error') ) {
            $errorMessage = $response->error->message . ' (' . $response->error->code . ')';
        } else {
            $events = $response->events; //array
        }

        $this->context->smarty->assign(array(
            'logo' =>  __PS_BASE_URI__ . 'modules' . '/' . $this->name .'/views/img/mandae.png',
            'error' => $errorMessage,
            'events' => $events,
            'trackingCode' => $trackingCode,
        ));

        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->setTemplate('module:mandae/views/templates/front/trackings.tpl');
        } else {
            $this->setTemplate('tracking.tpl');
        }


    }

    public function setMedia()
    {
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $uri = '/modules/' . $this->module->name . '/assets/css/' . $this->module->name . '.css';
            $this->registerStylesheet(sha1($uri), $uri, [
                'media' => 'all',
                'priority' => '1000',
            ]);
        } else {
            $uri = __PS_BASE_URI__ . 'modules/' . $this->module->name . '/assets/css/' . $this->module->name . '.css';
            $this->addCSS($uri);
        }

        return parent::setMedia();
    }

}