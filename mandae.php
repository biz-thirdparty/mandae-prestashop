<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__).'/classes/MandaeService.php');

class Mandae extends CarrierModule
{
    protected $_mandae;

    public $id_carrier;

    protected $shippingRate = array();

    protected $_carriers = array(
        'Mandaê' => 'mandae',
    );

    public $mandae_shipping = array(
        'expresso' => 'Expresso',
        'rapido' => 'Rápido',
        'economico' => 'Econômico',
    );

    public function __construct()
    {
        $this->name = 'mandae';
        $this->tab = 'shipping_logistics';
        $this->version = '0.1.1';
        $this->author = 'Mandaê';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        $this->link = new Link();

        parent::__construct();

        $this->displayName = $this->l('Mandaê');
        $this->description = $this->l('Economize Tempo e Dinheiro com Mandaê, a melhor solução logística para E-commerce; coleta, empacotamento e distribuição com segurança e velocidade');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    function install()
    {
        if (
            parent::install() == false
            || $this->registerHook('extraCarrier') == false // 1.6
            || $this->registerHook('updateCarrier') == false // 1.6
            || $this->registerHook('actionCarrierUpdate') == false // 1.7
            || $this->registerHook('displayAfterCarrier') == false // 1.7
        )
            return false;

        $this->installCarrier();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }
        // Uninstall Carriers
        Db::getInstance()->update('carrier', array('deleted' => 1), 'external_module_name LIKE "%'.$this->name.'%"');

        if (!parent::uninstall()
            || !$this->unregisterHook('extraCarrier') // 1.6
            || !$this->unregisterHook('updateCarrier') // 1.6
            || !$this->unregisterHook('actionCarrierUpdate') //1.7
            || !$this->unregisterHook('displayAfterCarrier')
        )
            return false;


        return true;
    }

    private function installCarrier()
    {
        $url = $this->link->getModuleLink('mandae', 'tracking', array(), Configuration::get('PS_SSL_ENABLED'), null, null, false);
        $params = 'object=@';
        $trackingUrl = (strpos($url, '?') === false) ? $url . '?' . $params : $url . '&' . $params;
            $defaultConfig = array(
                'active' => true,
                'deleted' => 0,
                'delay' => array('br' => '%d dias úteis'),
                'shipping_handling' => true,
                'range_behavior' => 0,
                'is_module' => true,
                'external_module_name' => $this->name,
                'shipping_external' => true,
                'need_range' => true,
                'url' => $trackingUrl,
            );

            $servicesArray = array();
            foreach ($this->mandae_shipping as $serviceCode => $service)
                $servicesArray[] = array(
                    "name" => 'Mandaê - ' . $service
                );

            foreach ($servicesArray as $config) {
                $config = array_merge($defaultConfig, $config);
                $this->createServices($config);
            }
        }

    /**
     *
     * @param $config
     * @return boolean
     */
    public function createServices($config)
    {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->url = $config['url'];
        $carrier->active = $config['active'];
        $carrier->deleted = $config['deleted'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language) {
            $carrier->delay[(int)$language['id_lang']] = $config['delay']['br'];
        }

        if ($carrier->add()) {

            Configuration::updateValue("PS_MANDAE_CARRIER_{$carrier->id}", $config['name']);

            $zones = Zone::getZones(true);
            foreach ($zones as $zone) {
                $carrier->addZone((int) $zone['id_zone']);
            }
            $groups = Group::getGroups($this->context->language->id);
            foreach ($groups as $group) {
                Db::getInstance()
                    ->insert(
                        'carrier_group',
                        array(
                            'id_carrier' => (int) $carrier->id,
                            'id_group'   => (int) $group['id_group'],
                        )
                    );
            }

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = 0;
            $rangePrice->delimiter2 = 30000;
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = 0;
            $rangeWeight->delimiter2 = 30000;
            $rangeWeight->add();

            $source = $logoPath = _PS_MODULE_DIR_ . $this->name.'/views/img/mandae.png';

            foreach ($languages as $language) {
                Tools::copy($source, _PS_SHIP_IMG_DIR_ . $carrier->id . '.jpg');
                Tools::copy($source, _PS_TMP_IMG_DIR_ . 'carrier_mini_'.$carrier->id . '_' . $language['id_lang'] . '.png');
            }

            return (int) ($carrier->id);
        }

        return false;
    }


    /**
     * Content for admin
     */
    public function getContent()
    {
        $this->context->controller->addCss($this->_path . 'assets/css/admin.css', 'all');

        if (Tools::isSubmit('btnSubmit')) {
            $this->_postValidation();
            if (!count($this->_postErrors))
                $this->_postProcess();
            else
                foreach ($this->_postErrors as $err)
                    $this->_html .= $this->displayError($err);
        } else
            $this->_html .= '<br />';

        $this->_html .= $this->_displayInfos();
        $this->_html .= $this->renderForm();

        return $this->_html;
    }

    /**
     * Validate Form admin
     */
    protected function _postValidation()
    {
        if (Tools::isSubmit('btnSubmit')) {
            $MANDAE_TOKEN = Tools::getValue('MANDAE_TOKEN');

            if (!$MANDAE_TOKEN) {
                $this->_postErrors[] = $this->l('É preciso colocar as informações da loja para ativar o módulo!');
            }
        }
    }

    /**
     * Submit form admin
     */
    protected function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {

            Configuration::updateValue('MANDAE_TOKEN', Tools::getValue('MANDAE_TOKEN'));
            Configuration::updateValue('MANDAE_STATUS', Tools::getValue('MANDAE_STATUS'));
            Configuration::updateValue('MANDAE_DEBUG', Tools::getValue('MANDAE_DEBUG'));
            Configuration::updateValue('MANDAE_SANDBOX', Tools::getValue('MANDAE_SANDBOX'));
            Configuration::updateValue('MANDAE_METHOD_TITLE', Tools::getValue('MANDAE_METHOD_TITLE'));
            Configuration::updateValue('MANDAE_DECLARED_VALUE', Tools::getValue('MANDAE_DECLARED_VALUE'));
            Configuration::updateValue('MANDAE_ADDITIONAL_DEADLINE', Tools::getValue('MANDAE_ADDITIONAL_DEADLINE'));
            Configuration::updateValue('MANDAE_HANDLING_TYPE', Tools::getValue('MANDAE_HANDLING_TYPE'));
            Configuration::updateValue('MANDAE_HANDLING_FEE', Tools::getValue('MANDAE_HANDLING_FEE'));
            Configuration::updateValue('MANDAE_DEFAULT_VALUES', Tools::getValue('MANDAE_DEFAULT_VALUES'));
            Configuration::updateValue('MANDAE_DEFAULT_WIDTH', Tools::getValue('MANDAE_DEFAULT_WIDTH'));
            Configuration::updateValue('MANDAE_DEFAULT_HEIGHT', Tools::getValue('MANDAE_DEFAULT_HEIGHT'));
            Configuration::updateValue('MANDAE_DEFAULT_DEPTH', Tools::getValue('MANDAE_DEFAULT_DEPTH'));
            Configuration::updateValue('MANDAE_DEFAULT_WEIGHT', Tools::getValue('MANDAE_DEFAULT_WEIGHT'));
            Configuration::updateValue('MANDAE_DEFAULT_UNITY', Tools::getValue('MANDAE_DEFAULT_UNITY'));
            Configuration::updateValue('MANDAE_DEFAULT_WEIGHT_UNITY', Tools::getValue('MANDAE_DEFAULT_WEIGHT_UNITY'));
        }
        $this->_html .= $this->displayConfirmation($this->l('Configurações Atualizadas'));
    }

    /**
     * Display infos admin
     * @return mixed
     */
    protected function _displayInfos()
    {
        return $this->display(__FILE__, 'infos.tpl');
    }

    /**
     * Render form admin
     * @return mixed
     */
    public function renderForm()
    {
        $this->context->smarty->assign('module_dir', _PS_MODULE_DIR_ . 'maxipago/');
        $this->context->smarty->assign('action_post', Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']));

        $this->context->smarty->assign('MANDAE_STATUS', Configuration::get('MANDAE_STATUS'));
        $this->context->smarty->assign('MANDAE_TOKEN', Configuration::get('MANDAE_TOKEN'));
        $this->context->smarty->assign('MANDAE_METHOD_TITLE', Configuration::get('MANDAE_METHOD_TITLE'));
        $this->context->smarty->assign('MANDAE_SANDBOX', Configuration::get('MANDAE_SANDBOX'));
        $this->context->smarty->assign('MANDAE_DECLARED_VALUE', Configuration::get('MANDAE_DECLARED_VALUE'));
        $this->context->smarty->assign('MANDAE_ADDITIONAL_DEADLINE', Configuration::get('MANDAE_ADDITIONAL_DEADLINE'));
        $this->context->smarty->assign('MANDAE_HANDLING_TYPE', Configuration::get('MANDAE_HANDLING_TYPE'));
        $this->context->smarty->assign('MANDAE_HANDLING_FEE', Configuration::get('MANDAE_HANDLING_FEE'));
        $this->context->smarty->assign('MANDAE_DEBUG', Configuration::get('MANDAE_DEBUG'));
        $this->context->smarty->assign('MANDAE_DEFAULT_VALUES', Configuration::get('MANDAE_DEFAULT_VALUES'));
        $this->context->smarty->assign('MANDAE_DEFAULT_WIDTH', Configuration::get('MANDAE_DEFAULT_WIDTH'));
        $this->context->smarty->assign('MANDAE_DEFAULT_HEIGHT', Configuration::get('MANDAE_DEFAULT_HEIGHT'));
        $this->context->smarty->assign('MANDAE_DEFAULT_DEPTH', Configuration::get('MANDAE_DEFAULT_DEPTH'));
        $this->context->smarty->assign('MANDAE_DEFAULT_WEIGHT', Configuration::get('MANDAE_DEFAULT_WEIGHT'));
        $this->context->smarty->assign('MANDAE_DEFAULT_UNITY', Configuration::get('MANDAE_DEFAULT_UNITY'));
        $this->context->smarty->assign('MANDAE_DEFAULT_WEIGHT_UNITY', Configuration::get('MANDAE_DEFAULT_WEIGHT_UNITY'));

        $this->context->smarty->assign('action', Tools::getValue('act'));

        return $this->display(__PS_BASE_URI__ . 'modules/mandae', 'views/templates/admin/settings.tpl');
    }


    public function getOrderShippingCostExternal($params)
    {
       return false;
    }

    /**
     * Receive an value and convert to "kg" or "cm"
     *
     * @param $args
     * @return array
     */
    public function convertMeasures($value, $unity, $decimals = 0)
    {
        $formulas = array(
            'g' => 0.001,
            'lb' => 0.45359237,
            'm' => 100,
            'in' => 2.54,
        );

        if (isset($formulas[$unity])) {
            $value = $value * $formulas[$unity];
        }

        return number_format($value, $decimals,'.', '');
    }

    /**
     * @param $params
     * @param $cost
     * @return string
     */
    public function getShippingRate($params)
    {
        if (empty($this->shippingRate)) {
            $useDefaultValues = Configuration::get('MANDAE_DEFAULT_VALUES');

            $cart = new Cart($this->context->cookie->id_cart);
            $products = $cart->getProducts(true, false, null);

            $address = new Address ($params->id_address_delivery);
            $postcode = preg_replace("/[^0-9]/", "", $address->postcode);

            $subtotal = $products[0]['total_wt'];
            $i = 0;
            for ($i = 1; $i < count($products); $i++) {
                $subtotal += $products[$i]['total_wt'];
            }

            $i = 0;
            $width = $products[0]['width'];
            for ($i = 1; $i < count($products); $i++) {
                if ($products[$i]['width'] > $width)
                    $width = $products[$i]['width'];
            }

            if (($width == null || $width == 0) && $useDefaultValues == 'yes')
                $width = Configuration::get('MANDAE_DEFAULT_WIDTH');

            $i = 0;
            $height = $products[0]['height'];
            for ($i = 1; $i < count($products); $i++) {
                $height += $products[$i]['height'];
            }

            if (($height == null || $height == 0) && $useDefaultValues == 'yes')
                $height = Configuration::get('MANDAE_DEFAULT_HEIGHT');

            $i = 0;
            $length = $products[0]['depth'];
            for ($i = 1; $i < count($products); $i++) {
                if ($products[$i]['depth'] > $length)
                    $length = $products[$i]['depth'];
            }

            if (($length == null || $length == 0) && $useDefaultValues == 'yes')
                $length = Configuration::get('MANDAE_DEFAULT_DEPTH');

            $i = 0;
            $weight = $products[0]['weight'];
            for ($i = 1; $i < $products[$i]; $i++) {
                $weight += $products[$i]['weight'];
            }

            if ($weight == null || $weight == 0 && $useDefaultValues == 'yes')
                $weight = Configuration::get('MANDAE_DEFAULT_WEIGHT');

            $url = MandaeService::getUrl() . 'postalcodes/' . $postcode . '/rates';

            $declaredValue = Configuration::get('MANDAE_DECLARED_VALUE') ? $subtotal : '0.00';

            $defaultLengthUnity = Configuration::get('MANDAE_DEFAULT_UNITY');
            $defaultMassUnity = Configuration::get('MANDAE_DEFAULT_WEIGHT_UNITY');
            $args = array(
                'declaredValue' => $declaredValue,
                'weight' => $this->convertMeasures($weight, $defaultMassUnity, 3),
                'height' => $this->convertMeasures($height, $defaultLengthUnity),
                'width' => $this->convertMeasures($width, $defaultLengthUnity),
                'length' => $this->convertMeasures($length, $defaultLengthUnity)
            );
            $data = json_encode($args);
            $result = json_decode(MandaeService::request($url, 'POST', $data));

            $this->shippingRate = $result;

            MandaeService::log('E: ' . $url . '. DATA:' . $data);
            MandaeService::log('R:' . json_encode($result));
        } else {
            $result = $this->shippingRate;
        }

        return $result;
    }

    /**
     * @param Cart $cart
     * @param $shipping_cost
     * @return bool|int
     */
    public function getOrderShippingCost($cart, $shipping_cost)
    {
        $shippingRate = $this->getShippingRate($cart);

        if (
            property_exists($shippingRate, 'shippingServices')
            && !empty($shippingRate->shippingServices)
        ) {
            $serviceName = Configuration::get('PS_MANDAE_CARRIER_' . $this->id_carrier);
            if ($serviceName) {
                foreach ($shippingRate->shippingServices as $service) {
                    if ($service->name == str_replace('Mandaê - ', '', $serviceName)) {
                        return $service->price + $shipping_cost + $this->getHandlingFee();
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param $shipping_cost
     * @return mixed
     */
    public function getHandlingFee()
    {
        $cost = 0;
        $mandaeHandling = Configuration::get('MANDAE_HANDLING_TYPE');
        $mandaeHandlingFee = Configuration::get('MANDAE_HANDLING_FEE');
        $cart = new Cart($this->context->cookie->id_cart);
        $cartTotal = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS, null, null, false);

        if ($mandaeHandling == 'fixed') {
            $cost = $mandaeHandlingFee;
        } else if ($mandaeHandling == 'percentage') {
            $cost = $cartTotal * ($mandaeHandlingFee/100);
        }
        return $cost;
    }

    public function hookUpdateCarrier($params)
    {
        $this->hookActionCarrierUpdate($params);
    }

    public function hookActionCarrierUpdate($params)
    {
        $carrier = $params['carrier'];
        Configuration::updateValue("PS_MANDAE_CARRIER_{$carrier->id}", $carrier->name);
    }

    /**
     *
     * @global type $smarty
     * @param type $params
     */
    public function hookExtraCarrier($params)
    {
        global $smarty;
        $cart = isset($params['cart']) ? $params['cart'] : false;
        if ($cart) {
            $additionalDeadline = (int) Configuration::get('MANDAE_ADDITIONAL_DEADLINE');
            $shippingRate = $this->getShippingRate($cart);

            $deliveryDays = array();
            if (
                property_exists($shippingRate, 'shippingServices')
                && !empty($shippingRate->shippingServices)
            ) {
                foreach ($shippingRate->shippingServices as $service) {
                    array_push($deliveryDays, array('name' => $service->name, 'days' => $service->days));
                }
                if (!empty($deliveryDays)) {
                    $this->context->smarty->assign('add_deadline', $additionalDeadline);
                    $this->context->smarty->assign('shipping_rate', $shippingRate);
                    $this->context->smarty->assign('delivery_days', json_encode($deliveryDays));

                    return $this->display(__FILE__, 'estimate.tpl');
                }

            }

        }

    }

    public function hookDisplayAfterCarrier($params)
    {
        return $this->hookExtraCarrier($params);
    }
}