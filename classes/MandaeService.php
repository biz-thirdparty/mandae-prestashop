<?php

class MandaeService extends ObjectModel
{
    const URL_API = 'https://api.mandae.com.br/v2/';
    const SANDBOX_URL_API = 'https://sandbox.api.mandae.com.br/v2/';

    public static function log($message, $severity = 1)
    {
        $debug = Configuration::get('MANDAE_DEBUG');
        if ($debug) {
            if (is_array($message) || is_object($message)) {
                $message = json_encode($message);
            }
            if (version_compare(_PS_VERSION_, '1.7', '>=')) {
                PrestaShopLogger::addLog($message, $severity, null, 'Mandae', 1, true);
            } else {
                Logger::addLog($message, $severity, null, 'Mandae', 1, true);
            }
        }
    }

    public static function getUrl()
    {
        return (Configuration::get('MANDAE_SANDBOX') == 'test') ? self::SANDBOX_URL_API : self::URL_API;
    }

    /**
     * @param $url
     * @param string $method
     * @param string $data
     *
     * @return string
     */
    public static function request($url, $method = 'GET', $data = '')
    {
        $headers = array(
            'Content-Type: application/json',
            'Authorization: ' . Configuration::get('MANDAE_TOKEN')
        );

        $curl = curl_init($url);
        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, true);
        }

        if ($data) {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;

    }
}